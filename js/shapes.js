class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class Geometry {
    draw() { }
    calculate() { }
}
class StartContainer extends Geometry {
    constructor(strp, dx, dy, canvas, img) {
        super();
        this.points = [];
        this.strp = strp;
        this.dx = dx;
        this.dy = dy;
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.img = img;
    }
    // protected calculate(){
    //     this.points.push(new Point(this.strp.x+(this.l/2)+(this.w/2),this.strp.x+(this.l/2)-(this.w/2)));
    //     this.points.push(new Point(this.strp.x+(this.l/2)+(this.w/2),this.strp.x+(this.l/2)-(this.w/2)));
    //     this.points.push(new Point(this.strp.x+(this.l/2)+(this.w/2),this.strp.x+(this.l/2)-(this.w/2)));
    //     this.points.push(new Point(this.strp.x+(this.l/2)+(this.w/2),this.strp.x+(this.l/2)-(this.w/2))); 
    // }
    draw() {
        this.context.save();
        this.context.translate(this.strp.x * lscale, this.strp.y * lscale);
        this.context.scale(1, -1);
        this.context.beginPath();
        this.context.drawImage(this.img, -this.dx / 2 * lscale, -this.dy / 2 * lscale, this.dx * lscale, this.dy * lscale);
        this.context.restore();
    }
    isinside(point) {
        if (point.x > this.strp.x * lscale - this.dx / 2 * lscale && point.x < this.strp.x * lscale + this.dx / 2 * lscale && point.y > this.strp.y * lscale - this.dy / 2 * lscale && point.y < this.strp.y * lscale + this.dy / 2 * lscale) {
            return true;
        }
        else {
            return false;
        }
    }
}
//# sourceMappingURL=shapes.js.map