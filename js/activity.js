const canvas = document.getElementById('canvas_activity1');
const ctx = canvas.getContext('2d');
const rect = canvas.getBoundingClientRect();
var lscale;
var scene = new Scene;
var startimg = new Image();
startimg.src = "./images/startcontainer.png";
var img2 = new Image();
img2.src = "./images/meter.png";
var img3 = new Image();
img3.src = "./images/roundflask.png";
var img4 = new Image();
img4.src = "./images/connector2.png";
var img5 = new Image();
img5.src = "./images/flask1.png";
var img6 = new Image();
img6.src = "./images/connector1.png";
var img7 = new Image();
img7.src = "./images/rectangle.png";
var img8 = new Image();
img8.src = "./images/pipe.png";
var img9 = new Image();
img9.src = "./images/flask2.png";
var shape1 = new StartContainer(new Point(1000, 100), 375, 125, canvas, startimg);
scene.add(shape1);
var meter = new StartContainer(new Point(1170, 225), 110, 125, canvas, img2);
scene.add(meter);
var roundflask = new StartContainer(new Point(790, 48), 53, 56, canvas, img3);
scene.add(roundflask);
var connector2 = new StartContainer(new Point(778, 111), 30, 70, canvas, img4);
scene.add(connector2);
var flask1 = new StartContainer(new Point(776, 191), 56, 88, canvas, img5);
scene.add(flask1);
var connector1 = new StartContainer(new Point(810, 260), 95, 45, canvas, img6);
scene.add(connector1);
var rectangle = new StartContainer(new Point(774, 418), 63, 265, canvas, img7);
scene.add(rectangle);
var pipe = new StartContainer(new Point(850, 565), 177, 28, canvas, img8);
scene.add(pipe);
var flask2 = new StartContainer(new Point(930, 510), 71, 80, canvas, img9);
scene.add(flask2);
function drag_geo(x, y) {
    for (let i = 0; i < scene.container.length; i++) {
        if (scene.container[i].geo.isinside(new Point(x, y))) {
            scene.container[i].geo.strp = new Point(x, y);
            scene.draw();
            console.log(x, y);
        }
    }
}
var geo;
geo = "drag";
//# sourceMappingURL=activity.js.map