window.onload = function () {
    canvas_size();
    canvas_border();
    canvas_mapping();
    scene.draw();
};
window.onresize = function () {
    canvas_size();
    canvas_border();
    canvas_mapping();
    scene.draw();
};
function canvas_size() {
    canvas.width = window.innerWidth * 0.97;
    canvas.height = canvas.width * 1080.0 / 1920.0 * 0.65;
    lscale = canvas.width / 1920.0;
}
function canvas_border() {
    ctx.beginPath();
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = 4;
    ctx.stroke();
}
function canvas_mapping() {
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
}
const tb = document.getElementById('t1');
canvas.addEventListener("mousemove", mousemove);
canvas.addEventListener("mousedown", mousedowm);
canvas.addEventListener("mouseup", mouseup);
canvas.addEventListener("touchmove", mousetouch);
canvas.addEventListener("click", click);
var drag = false;
function mousedowm(e) {
    drag = true;
}
function mouseup(e) {
    drag = false;
}
function mousetouch(e) {
    let x = Math.round((e.clientX - rect.x) / lscale);
    let y = Math.round((canvas.height - (e.clientY - rect.y)) / lscale);
    drag_geo(x, y);
    tb.value = `${Math.round(x / lscale)}, ${Math.round(y / lscale)}`;
}
function mousemove(e) {
    let x = Math.round((e.clientX - rect.x) / lscale);
    let y = Math.round((canvas.height - (e.clientY - rect.y)) / lscale);
    if (drag) {
        drag_geo(x, y);
    }
    tb.value = `${Math.round(x / lscale)}, ${Math.round(y / lscale)}`;
}
function click(e) {
    scene.add(new StartContainer(new Point(e.clientX, e.clientY), 177, 28, canvas, img8));
    scene.draw();
}
//# sourceMappingURL=index.js.map